package dawsoncollege.android.adapterexample

import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import dawsoncollege.android.adapterexample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var mutableListOfLetters: MutableList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /*
         * This is the datasource the Adapter will interact with.
         *
         * In this case, the datasource is a MutableList we created from a string array resource
         */
        mutableListOfLetters = resources.getStringArray(R.array.letters).toMutableList()

        /*
         * This is the Adapter, it's the middle-man between the datasource and the AdapterView.
         *
         * The AdapterView uses the Adapter whenever it needs a new item (e.g. when the user scrolls
         * down and we need to show the next item in the list).
         * This is how it conceptually happens :
         * 1. AdapterView requests the next item from its Adapter
         * 2. The Adapter requests the next item from its datasource
         * 3. Given the new item from its datasource, the Adapter creates a View for that item
         * 4. The View is returned to the AdapterView
         *
         * Another important point : the second parameter of the constructor is the layout that is
         * used for every item. It is what the Adapter uses to create the View for each item (see
         * step 3 in the enumeration above).
         *
         * In this case, the Adapter we use is an ArrayAdapter (simple Adapter for an array/list)
         */
        val adapter = ArrayAdapter(this, R.layout.letter_text_view, mutableListOfLetters)

        // Try running the application with the following adapters instead:
        // - This one uses another layout we defined
//        val adapter = ArrayAdapter(this, R.layout.big_letter_text_view, mutableListOfLetters)
        // - This one uses an Android provided layout
//        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, mutableListOfLetters)


        /*
         * This is the AdapterView, its responsibility is to display the View objects given to it by
         * its Adapter.
         *
         * In this case, the AdapterView we use is a ListView (displays its View in a scrollable list)
         */
        binding.listViewOfLetters.adapter = adapter

        binding.shuffleBtn.setOnClickListener {
            mutableListOfLetters.shuffle()

            /*
             * Since the Adapter is not always querying its datasource to see if the data changed,
             * it's the developer's responsibility to call `notifyDataSetChanged()`. This function
             * notifies all observers of the Adapter, to let them know the underlying data changed.
             *
             * The AdapterView is one of the observers and will then redraw its containing Views
             */
            adapter.notifyDataSetChanged()
        }
    }

}